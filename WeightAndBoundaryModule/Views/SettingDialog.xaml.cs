﻿using Afterbunny.Windows.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using UniversalWeightSystem.Core;
using WeightAndBoundaryModule.Annotations;
using WeightAndBoundaryModule.ViewModels;

namespace WeightAndBoundaryModule.Views
{
    /// <summary>
    /// Interaction logic for SettingDialog.xaml
    /// </summary>
    public partial class SettingDialog : Window, INotifyPropertyChanged
    {
        private List<CalculationMethodItemViewModel> _calculationMethodItemViewModels;

        public bool ShouldSave { get; private set; }

        public SettingDialog(List<WeightCollectionViewModel> weightCollectionViewModels,
            List<CalculationMethodItemViewModel> calculationMethodItemViewModels,
            List<BoundaryViewModel> boundaryViewModels, bool canEditWeights, bool canEditBoundaries,
            string[] inputNames, string[] weightNames)
        {
            WeightCollectionViewModels = weightCollectionViewModels;
            CalculationMethodItemViewModels = calculationMethodItemViewModels;
            BoundaryViewModels = boundaryViewModels;
            CanEditWeights = canEditWeights;
            CanEditBoundaries = canEditBoundaries;
            InputNames = inputNames;
            WeightNames = weightNames;

            // Generate method hint
            {
                var firstOutputName = CalculationMethodItemViewModels[0].OutputName;
                if (inputNames.Length == 1)
                {
                    var methodHint = $"{firstOutputName} = input.{inputNames[0]} * weight.{weightNames[0]}";
                    if (weightNames.Length > 1) methodHint += $" + weight.{weightNames[1]}";
                    MethodHint = methodHint;
                }
                else
                {
                    var hint =
                        $"{firstOutputName} = pow(input.{inputNames[0]}, 2) * weight.{weightNames[0]} + input.{inputNames[1]}";
                    if (weightNames.Length > 1) hint += $" * weight.{weightNames[1]}";
                    MethodHint = hint;
                }
            }

            InitializeComponent();
            DataContext = this;
        }

        public string MethodHint { get; }

        public List<WeightCollectionViewModel> WeightCollectionViewModels { get; }

        public List<CalculationMethodItemViewModel> CalculationMethodItemViewModels
        {
            get => _calculationMethodItemViewModels;
            private set
            {
                _calculationMethodItemViewModels = value;
                OnPropertyChanged();
            }
        }

        public List<BoundaryViewModel> BoundaryViewModels { get; }

        public bool CanEditWeights { get; }

        public bool CanEditBoundaries { get; }

        public string[] InputNames { get; }

        public string[] WeightNames { get; }

        private void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            if (!CanEditBoundaries && !CanEditWeights)
            {
                ShowErrorMessage("请先登录");
                return;
            }

            if (CanEditWeights)
            {
                var errorMessage = VerifyMethods();
                if (errorMessage != null)
                {
                    ShowErrorMessage(errorMessage);
                    return;
                }
            }

            if (CanEditBoundaries)
            {
                var firstInvalidBoundary = BoundaryViewModels.FirstOrDefault(b =>
                    b.Enable && (b.Standard <= b.Min || b.Max <= b.Min || b.Max <= b.Standard));
                if (firstInvalidBoundary != null)
                {
                    ShowErrorMessage($"请确保[{firstInvalidBoundary.Name}]上下限和标准值的合法性,即上限>标准值>下限");
                    return;
                }
            }

            ShouldSave = true;

            Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// ErrorMessage:
        /// null for no error
        /// </returns>
        private string VerifyMethods()
        {
            // Verify method definitions
            var firstUndefinedMethod =
                CalculationMethodItemViewModels.FirstOrDefault(m => string.IsNullOrWhiteSpace(m.MethodDefinition));
            if (firstUndefinedMethod != null)
            {
                return $"方法[{firstUndefinedMethod.OutputName}]未定义";

            }

            var random = new Random(42);
            var inputs = InputNames.ToDictionary(n => n, n => random.NextDouble());
            var weights = WeightNames.ToDictionary(n => n, n => random.NextDouble());
            var methods = CalculationMethodItemViewModels.ToDictionary(m => m.OutputName, m => m.MethodDefinition);
            var (output, exceptions) = WeightWeaver.Weight(inputs, weights, methods);
            if (exceptions?.Any() == true)
            {
                var firstEx = exceptions.First();
                return $"计算方法[{firstEx.Key}]定义有误: {firstEx.Value}";

            }

            return null;
        }

        private void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "错误");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnExportMethodsButtonClicked(object sender, RoutedEventArgs e)
        {
            var methodError = VerifyMethods();
            if (methodError != null)
            {
                ShowErrorMessage($"存在无效的方法定义: {methodError}");
                return;
            }

            var exportDir = FileSystemHelper.GetDirFromDialog(cacheFile: Path.Combine(
                KnownFolders.GetPath(KnownFolder.History), "DistributedVisionRunner.Cache.MethodZipDir"));
            if (!string.IsNullOrEmpty(exportDir))
            {
                var tempDir = Path.Combine(exportDir, Guid.NewGuid().ToString());
                Directory.CreateDirectory(tempDir);
                foreach (var method in CalculationMethodItemViewModels)
                {
                    var path = Path.Combine(tempDir, $"{method.OutputName}.calc");
                    File.WriteAllText(path, method.MethodDefinition);
                }

                ZipFile.CreateFromDirectory(tempDir,
                    Path.Combine(exportDir, $"ExportedMethods_{DateTime.Now:yyyyMMddHHmmss}.dvrz"));
                Helper.DeleteDirectory(tempDir);
                Process.Start("explorer.exe", exportDir);
            }

        }

        private void OnImportMethodsButtonClicked(object sender, RoutedEventArgs eventArgs)
        {
            var archive = FileSystemHelper.GetFileFromDialog(pattern: (new[] { "dvrz" }, "dvrz文件"),
                cacheFile: Path.Combine(KnownFolders.GetPath(KnownFolder.History),
                    "DistributedVisionRunner.Cache.MethodZipFile"));
            if (!string.IsNullOrEmpty(archive))
            {
                var incomingMethodNames = new HashSet<string>(ZipFile.OpenRead(archive).Entries
                    .Where(e => e.Name.EndsWith(".calc")).Select(e => Path.GetFileNameWithoutExtension(e.Name)));
                var updatingMethodNames = new List<string>();
                foreach (var method in CalculationMethodItemViewModels)
                {
                    if (incomingMethodNames.Contains(method.OutputName)) updatingMethodNames.Add(method.OutputName);
                }

                // Extract to temp dir
                var tempDir = Path.Combine(Directory.GetParent(archive).FullName, Guid.NewGuid().ToString());
                Directory.CreateDirectory(tempDir);
                ZipFile.ExtractToDirectory(archive, tempDir);

                foreach (var name in updatingMethodNames)
                {
                    var fileName = $"{name}.calc";
                    var src = Path.Combine(tempDir, fileName);
                    var newDef = File.ReadAllText(src);
                    var methodToUpdate =
                        CalculationMethodItemViewModels.First(m => m.OutputName == name);
                    methodToUpdate.MethodDefinition = newDef;

                }

                OnPropertyChanged(nameof(CalculationMethodItemViewModels));

                // Remove temp dir
                Helper.DeleteDirectory(tempDir);

                // Prompt user which methods are updated
                MessageBox.Show($"更新了共计{updatingMethodNames.Count}个方法的定义: \n"
                                + string.Join("\n   ", updatingMethodNames));

            }
        }
    }
}

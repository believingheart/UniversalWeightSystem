﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Xml.Serialization;
using UniversalWeightSystem.Core;
using WeightAndBoundaryModule.Views;

namespace WeightAndBoundaryModule.ViewModels
{
    public class WeightAndBoundaryViewModel
    {
        private readonly string[] _inputNames;
        private readonly int _cavityCount;
        private bool _canWeight = true;
        private List<WeightCollectionViewModel> _weightCollections;
        private List<CalculationMethodItemViewModel> _methods;
        private Dictionary<string, BoundaryViewModel> _boundaryLookupCache;

        /// <summary>
        /// 用户是否可以编辑补偿
        /// </summary>
        public bool CanEditWeights { get; set; }

        /// <summary>
        /// 用户是否可以编辑上下限
        /// </summary>
        public bool CanEditBoundaries { get; set; }

        /// <summary>
        /// 弹出配置窗口
        /// </summary>
        public ICommand ConfigCommand { get; }

        public WeightAndBoundaryViewModel(string projectName, string[] inputNames, string[] weightNames, string[] outputNames, int cavityCount, Action<string> fatalMethod)
        {
            List<BoundaryViewModel> boundaries;
            _inputNames = inputNames;
            _cavityCount = cavityCount;
            var projectDir = Path.Combine("c:/ProgramData/WeightAndBoundaryModule", projectName);
            var weightsDir = Path.Combine(projectDir, "Weights");
            var methodsDir = Path.Combine(projectDir, "Methods");

            // Load weights
            var weightProjectDir = Path.Combine(projectDir, "WeightSettings");
            Directory.CreateDirectory(weightProjectDir);

            void Fatal(string message)
            {
                fatalMethod.Invoke(message);
            }

            // Check weight files
            var (loadedWeights, newlyAddedWeights) = LoadWeights(weightsDir, weightNames, cavityCount);
            if (newlyAddedWeights != null && newlyAddedWeights.Any())
            {
                var weightNamesText = string.Join(", ", newlyAddedWeights);
                Fatal($"存在未配置的权重:{weightNamesText}");
                _canWeight = false;
            }
            _weightCollections = loadedWeights;

            // Check method files
            var (loadedMethods, missingMethods) = LoadMethods(methodsDir, outputNames);
            if (missingMethods.Any())
            {
                var missingMethodsText = string.Join(", ", missingMethods);
                Fatal($"存在未配置的权重方法:{missingMethodsText}");
                _canWeight = false;
            }

            _methods = loadedMethods;

            // try weight
            if (_canWeight)
            {

                var random = new Random(42);
                var inputs = inputNames.ToDictionary(n => n, n => random.NextDouble());
                var weights = weightNames.ToDictionary(n => n, n => random.NextDouble());
                var methods = _methods.ToDictionary(m => m.OutputName, m => m.MethodDefinition);
                var (output, exceptions) = WeightWeaver.Weight(inputs, weights, methods);
                if (exceptions?.Any() == true)
                {
                    var firstEx = exceptions.First();
                    Fatal($"计算方法[{firstEx.Key}]定义有误: {firstEx.Value}");
                    _canWeight = false;
                }

            }

            // Load boundaries
            void GenBoundaryLookupCache()
            {
                _boundaryLookupCache = new Dictionary<string, BoundaryViewModel>();
                foreach (var boundary in boundaries)
                {
                    _boundaryLookupCache[boundary.Name] = boundary;
                }
            }
            var boundaryDir = Path.Combine(projectDir, "Boundaries");
            Directory.CreateDirectory(boundaryDir);

            boundaries = new List<BoundaryViewModel>();
            foreach (var outputName in outputNames)
            {
                var fileName = $"{outputName}.boundary";
                var filePath = Path.Combine(boundaryDir, fileName);
                if (File.Exists(filePath))
                {
                    using (var reader = new StreamReader(filePath))
                    {
                        var serializer = new XmlSerializer(typeof(BoundaryViewModel));
                        boundaries.Add((BoundaryViewModel)serializer.Deserialize(reader));
                    }
                }
                else
                {
                    boundaries.Add(new BoundaryViewModel()
                    {
                        Name = outputName
                    });
                }
            }
            GenBoundaryLookupCache();

            ConfigCommand = new RelayCommand<object>(o =>
            {
                // Copy configs
                var weightCollectionsCopy = new List<WeightCollectionViewModel>();
                foreach (var weightCollection in _weightCollections)
                {
                    var weights = weightCollection.WeightItems.Select(i => new WeightItemViewModel()
                    {
                        Name = i.Name,
                        Weight = i.Weight
                    }).ToList();
                    weightCollectionsCopy.Add(new WeightCollectionViewModel(weights)
                    {
                        Index = weightCollection.Index,
                        NeedToSave = weightCollection.NeedToSave
                    });
                }

                var methodsCopy = _methods.Select(m => new CalculationMethodItemViewModel()
                { OutputName = m.OutputName, MethodDefinition = m.MethodDefinition }).ToList();

                var boundariesCopy = boundaries.Select(b => new BoundaryViewModel()
                {
                    Enable = b.Enable,
                    Max = b.Max,
                    Min = b.Min,
                    Standard = b.Standard,
                    Name = b.Name
                }).ToList();

                var dialog = new SettingDialog(weightCollectionsCopy, methodsCopy, boundariesCopy, CanEditWeights, CanEditBoundaries, inputNames, weightNames);
                dialog.ShowDialog();
                if (dialog.ShouldSave)
                {
                    if (CanEditWeights)
                    {
                        // Save weight collections
                        _weightCollections = dialog.WeightCollectionViewModels;

                        foreach (var weightCollection in _weightCollections)
                        {
                            var fileName = $"{weightCollection.Index:D3}.weight";
                            var filePath = Path.Combine(weightsDir, fileName);
                            var lines = weightCollection.WeightItems.Select(w => $"{w.Name} = {w.Weight}").ToArray();
                            File.WriteAllLines(filePath, lines);
                            weightCollection.NeedToSave = false;
                        }

                        // Save methods
                        _methods = dialog.CalculationMethodItemViewModels;
                        Directory.CreateDirectory(methodsDir);
                        foreach (var method in _methods)
                        {
                            var path = Path.Combine(methodsDir, $"{method.OutputName}.calc");
                            File.WriteAllText(path, method.MethodDefinition);
                        }

                        _canWeight = true;
                    }

                    if (CanEditBoundaries)
                    {
                        // Save boundaries
                        boundaries = dialog.BoundaryViewModels;
                        foreach (var boundary in boundaries)
                        {
                            var path = Path.Combine(boundaryDir, $"{boundary.Name}.boundary");
                            using (var writer = new StreamWriter(path))
                            {
                                var serializer = new XmlSerializer(boundary.GetType());
                                serializer.Serialize(writer, boundary);
                            }
                        }

                        GenBoundaryLookupCache();
                    }
                }
            });
        }

        /// <summary>
        /// 获取上下限
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public BoundaryViewModel GetBoundary(string name)
        {
            return _boundaryLookupCache[name];
        }


        /// <summary>
        /// 补偿
        /// </summary>
        /// <param name="inputs">输入的变量</param>
        /// <param name="cavity">穴位号, 注意是从1开始</param>
        /// <returns></returns>
        public Dictionary<string, double> Weight(Dictionary<string, double> inputs, int cavity)
        {
            if (!_canWeight) throw new InvalidOperationException("补偿系统的设置未完善, 补偿无法执行");
            if (cavity < 1 || cavity > _cavityCount) throw new ArgumentException($"穴位号的范围是[1, {_cavityCount}]", nameof(cavity));
            var firstUndefinedInputName = inputs.Keys.FirstOrDefault(inputName => !_inputNames.Contains(inputName));
            if (firstUndefinedInputName != null)
            {
                throw new ArgumentException($"未定义的输入变量[{firstUndefinedInputName}]", nameof(inputs));
            }

            var weightItems = _weightCollections.First(c => c.Index == cavity).WeightItems;
            var weightDict = weightItems.ToDictionary(i => i.Name, i => i.Weight);
            var scripts = _methods.ToDictionary(m => m.OutputName, m => m.MethodDefinition);

            var (weightedOutput, errors) = WeightWeaver.Weight(inputs, weightDict, scripts);

            return weightedOutput;
        }

        static (List<CalculationMethodItemViewModel> loadedMethods, List<string> missingMethods) LoadMethods(string methodDir, string[] outputNames)
        {
            Directory.CreateDirectory(methodDir);
            var methodFiles = Directory.GetFiles(methodDir).Where(p => p.EndsWith(".calc")).ToArray();

            var loadedMethods = new List<CalculationMethodItemViewModel>();
            var missingMethods = new List<string>();
            foreach (var outputName in outputNames)
            {
                var file = methodFiles.FirstOrDefault(f => Path.GetFileNameWithoutExtension(f) == outputName);
                if (file == null)
                {
                    missingMethods.Add(outputName);
                    loadedMethods.Add(new CalculationMethodItemViewModel() { OutputName = outputName });
                }
                else
                {
                    loadedMethods.Add(new CalculationMethodItemViewModel()
                    {
                        OutputName = outputName,
                        MethodDefinition = File.ReadAllText(file)
                    });
                }
            }


            return (loadedMethods, missingMethods);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weightProjectDir"></param>
        /// <param name="weightNames"></param>
        /// <param name="weightSetCount"></param>
        /// <returns>
        /// newlyDefinedWeights: null if no newly defined weights are found
        ///
        /// </returns>
        private static (List<WeightCollectionViewModel> weightCollection, string[] newlyDefinedWeights) LoadWeights(string weightDir, string[] weightNames, int weightSetCount)
        {
            Directory.CreateDirectory(weightDir);

            var expectedWeightFilePaths = Enumerable.Range(1, weightSetCount)
                .Select(index => Path.Combine(weightDir, $"{index:D3}.weight")).ToArray();

            var output = new List<WeightCollectionViewModel>();
            string[] newlyDefinedWeights = null;
            for (var setIndex = 0; setIndex < expectedWeightFilePaths.Length; setIndex++)
            {
                var filePath = expectedWeightFilePaths[setIndex];
                var fileExists = File.Exists(filePath);
                var (aSetOfWeightItems, newlyAddedWeights) = fileExists
                    ? ReadWeightsFromFile(filePath, weightNames)
                    : (CreateASetOfWeights(weightNames), weightNames);
                if (newlyAddedWeights.Length != 0)
                {
                    newlyDefinedWeights = newlyAddedWeights;
                }
                output.Add(new WeightCollectionViewModel(aSetOfWeightItems)
                { Index = setIndex + 1, NeedToSave = !fileExists });
            }



            return (output, newlyDefinedWeights);
        }

        private static (List<WeightItemViewModel> weightItems, string[] newlyAddWeights) ReadWeightsFromFile(string filePath, string[] weightNames)
        {
            var rawLines = File.ReadAllLines(filePath);
            var spaceFreeLines = rawLines.Select(line => Regex.Replace(line, @"\s+", "")).ToArray();
            // variableName=number
            var pattern = new Regex(@"^([a-zA-Z]\w*)=(-?[\d\.e]+)$");

            var userDefinedHashSet = new HashSet<string>(weightNames);
            var successfulReadWeights = new List<WeightItemViewModel>();
            foreach (var line in spaceFreeLines)
            {
                var matchResult = pattern.Match(line);
                if (!matchResult.Success) continue;
                var weightName = matchResult.Groups[1].Value;
                if (!userDefinedHashSet.Contains(weightName)) continue;
                var weightValueText = matchResult.Groups[2].Value;
                var isNumber = double.TryParse(weightValueText, out var weight);
                if (isNumber) successfulReadWeights.Add(new WeightItemViewModel() { Name = weightName, Weight = weight });
            }

            if (successfulReadWeights.Count == 0)
                return (CreateASetOfWeights(weightNames), weightNames);

            // Look for missing weightItems
            // And init missing weightItems with empty weight values
            var loadedHashSet = new HashSet<string>(successfulReadWeights.Select(w => w.Name));
            var missingWeights = weightNames.Where(name => !loadedHashSet.Contains(name))
                .Select(name => new WeightItemViewModel() { Name = name });
            var weightItemViewModels = missingWeights as WeightItemViewModel[] ?? missingWeights.ToArray();
            successfulReadWeights.AddRange(weightItemViewModels);

            var missingWeightNames = weightItemViewModels.Select(w => w.Name).ToArray();
            return (successfulReadWeights.OrderBy(w => w.Name).ToList(), missingWeightNames);
        }


        private static List<WeightItemViewModel> CreateASetOfWeights(string[] weightNames)
        {
            return weightNames.Select(name => new WeightItemViewModel { Name = name }).ToList();
        }


    }
}
﻿namespace WeightAndBoundaryModule.ViewModels
{
    public class BoundaryViewModel
    {
        public string Name { get; set; }

        public double Min { get; set; }

        public double Max { get; set; }

        public double Standard { get; set; }

        public bool Enable { get; set; }
    }
}
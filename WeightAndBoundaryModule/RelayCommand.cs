﻿using System;
using System.Windows.Input;

namespace WeightAndBoundaryModule
{
    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _action;

        public RelayCommand(Action<T> action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            T param = default;

            try
            {
                param = (T)parameter;
            }
            catch
            {
                // Don't care
            }

            _action.Invoke(param);
        }

        public event EventHandler CanExecuteChanged;
    }
}
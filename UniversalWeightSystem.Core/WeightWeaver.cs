﻿using IronPython.Hosting;
using Microsoft.Scripting;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace UniversalWeightSystem.Core
{
    public class WeightWeaver
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputs">输入的变量</param>
        /// <param name="weights">用于补偿的变量</param>
        /// <param name="scripts">定义的计算方法</param>
        /// <param name="inputName">用作python变量的输入变量名称</param>
        /// <param name="weightName">用作Python变量的补偿变量名称</param>
        /// <returns>补偿后的变量</returns>
        public static (Dictionary<string, double> output, Dictionary<string, Exception> exceptions) Weight(
            Dictionary<string, double> inputs,
            Dictionary<string, double> weights,
            Dictionary<string, string> scripts, string inputName = "input", string weightName = "weight")
        {
            void AssignPythonInputs(Dictionary<string, double> rawInputs, dynamic pyInputs)
            {
                var inputDict = (IDictionary<string, object>)pyInputs;
                foreach (var p in rawInputs)
                {
                    inputDict[p.Key] = p.Value;
                }
            }

            dynamic pythonInputs = new ExpandoObject();
            AssignPythonInputs(inputs, pythonInputs);

            dynamic pythonWeights = new ExpandoObject();
            AssignPythonInputs(weights, pythonWeights);

            var engine = Python.CreateEngine();
            var scope = engine.CreateScope();
            scope.SetVariable(inputName, pythonInputs);
            scope.SetVariable(weightName, pythonWeights);

            var output = new Dictionary<string, double>();
            var exceptions = new Dictionary<string, Exception>();

            foreach (var p in scripts)
            {
                var outputName = p.Key;
                var script = p.Value;
                var source = engine.CreateScriptSourceFromString(script, SourceCodeKind.Expression);
                try
                {
                    output[outputName] = source.Execute(scope);
                }
                catch (Exception e)
                {
                    exceptions[outputName] = e;
                }
            }

            return (output, exceptions);
        }



    }
}
﻿namespace UniversalWeightSystem.Core
{
    public class ProjectItem
    {
        public int WeightSets { get; set; }
        public string WeightNamesCsv { get; set; }
        public string InputNamesCsv { get; set; }
        public string OutputNamesCsv { get; set; }
    }
}
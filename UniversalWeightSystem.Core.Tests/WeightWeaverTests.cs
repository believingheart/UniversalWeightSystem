﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UniversalWeightSystem.Core.Tests
{
    [TestFixture]
    public class WeightWeaverTests
    {
        [Test]
        public void Weight_CalcResultShouldMatch()
        {
            var input = new Dictionary<string, double>()
            {
                ["a1"] = 2,
                ["a2"] = 3
            };
            // 2 * 100 + 3 * 200 = 800
            var expectedOutput = 800d;

            var weights = new Dictionary<string, double>()
            {
                ["x1"] = 100,
                ["x2"] = 200
            };

            var scripts = new Dictionary<string, string>()
            {
                ["y"] = "input.a1 * weight.x1 + input.a2 * weight.x2"
            };


            var actualOutput = WeightWeaver.Weight(input, weights, scripts);

            Assert.IsTrue(Math.Abs(expectedOutput - actualOutput.output["y"]) < double.Epsilon);
        }


        [Test]
        public void Weight_ExceptionsNotEmpty_InputVariablesAreMissing()
        {
            var input = new Dictionary<string, double>()
            {
                ["a1"] = 2,
            };
            // 2 * 100 + 3 * 200 = 800
            var expectedOutput = 800d;

            var weights = new Dictionary<string, double>()
            {
                ["x1"] = 100,
                ["x2"] = 200
            };

            var scripts = new Dictionary<string, string>()
            {
                ["y"] = "input.a1 * weight.x1 + input.a2 * weight.x2"
            };


            var (output, exceptions) = WeightWeaver.Weight(input, weights, scripts);

            Assert.IsTrue(exceptions?.Any() == true);
        }


        [Test]
        public void Weight_ExceptionsNotEmpty_WeightVariablesAreMissing()
        {
            var input = new Dictionary<string, double>()
            {
                ["a1"] = 2,
                ["a2"] = 3
            };
            // 2 * 100 + 3 * 200 = 800
            var expectedOutput = 800d;

            var weights = new Dictionary<string, double>()
            {
                ["x1"] = 100,
            };

            var scripts = new Dictionary<string, string>()
            {
                ["y"] = "input.a1 * weight.x1 + input.a2 * weight.x2"
            };


            var (output, exceptions) = WeightWeaver.Weight(input, weights, scripts);

            Assert.IsTrue(exceptions?.Any() == true);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Windows;
using WeightAndBoundaryModule.ViewModels;

namespace Dev
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public WeightAndBoundaryViewModel WeightAndBoundaryViewModel { get; set; }

        public double X1 { get; set; }

        public double X2 { get; set; }
        public int Cavity { get; set; }


        public static readonly DependencyProperty Y1Property = DependencyProperty.Register(
            "Y1", typeof(double), typeof(MainWindow), new PropertyMetadata(default(double)));

        public double Y1
        {
            get { return (double)GetValue(Y1Property); }
            set { SetValue(Y1Property, value); }
        }

        public static readonly DependencyProperty Y2Property = DependencyProperty.Register(
            "Y2", typeof(double), typeof(MainWindow), new PropertyMetadata(default(double)));

        public double Y2
        {
            get { return (double)GetValue(Y2Property); }
            set { SetValue(Y2Property, value); }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            WeightAndBoundaryViewModel = new WeightAndBoundaryViewModel("Dev", new[] { "x1", "x2" }, new[] { "w1", "w2" }, new[] { "y1", "y2" }, 4,
                LogMethod);

            WeightAndBoundaryViewModel.CanEditBoundaries = true;
            WeightAndBoundaryViewModel.CanEditWeights = true;
        }

        private void LogMethod(string message)
        {
            ErrorMessageTextBox.AppendText($"{DateTime.Now}> {message}\n");
        }

        private void OnTestButtonClicked(object sender, RoutedEventArgs e)
        {
            var inputs = new Dictionary<string, double>()
            {
                ["x1"] = X1,
                ["x2"] = X2,
            };


            try
            {
                var outputs = WeightAndBoundaryViewModel.Weight(inputs, Cavity);

                Y1 = outputs["y1"];
                Y2 = outputs["y2"];
            }
            catch (Exception ex)
            {
                LogMethod(ex.Message);
            }
        }
    }
}

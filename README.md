## 介绍
- 通用补偿系统(*UniversalWeightSystem.Core*)是为任意补偿方式开发的基础库
- 补偿以及上下限模块(*WeightAndBoundaryModule*)是基于通用补偿系统开发的WPF模块, 已经集成了补偿的定义, 保存, 加载和导入导出和权限管理功能

## 使用
1. UniversalWeightSystem.Core
- 只含有一个函数WeightWeaver.Weight
```cs
/// <summary>
/// 
/// </summary>
/// <param name="inputs">输入的变量</param>
/// <param name="weights">用于补偿的变量</param>
/// <param name="scripts">定义的计算方法</param>
/// <param name="inputName">用作python变量的输入变量名称<param>
/// <param name="weightName">用作Python变量的补偿变量名称<param>
/// <returns>补偿后的变量</returns>
public static (Dictionary<string, double> output, Dictionary<string, Exception> exceptions) Weight(
            Dictionary<string, double> inputs,
            Dictionary<string, double> weights,
            Dictionary<string, string> scripts, string inputName = "input", string weightName = "weight");
```
- 使用方法参考测试项目*UniversalWeightSystem.Core.Tests*
- Nuget包, 搜索*UniversalWeightSystem.Core*

2. WeightAndBoundaryModule
- 可用的类: *WeightAndBoundaryViewModel*
- 使用方法参考Dev项目

![Alt text](Images/preview.png?raw=true "视觉开发人员开发流程")
- Nuget包, 搜索*WeightAndBoundaryModule*